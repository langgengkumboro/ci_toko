-- phpMyAdmin SQL Dump
-- version 4.5.1
-- http://www.phpmyadmin.net
--
-- Host: 127.0.0.1
-- Generation Time: Feb 05, 2019 at 04:38 AM
-- Server version: 10.1.9-MariaDB
-- PHP Version: 7.0.1

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `toko_jaya_abadi`
--
CREATE DATABASE IF NOT EXISTS `toko_jaya_abadi` DEFAULT CHARACTER SET latin1 COLLATE latin1_swedish_ci;
USE `toko_jaya_abadi`;

-- --------------------------------------------------------

--
-- Table structure for table `barang`
--

CREATE TABLE `barang` (
  `kode_barang` varchar(5) NOT NULL,
  `nama_barang` varchar(150) NOT NULL,
  `harga_barang` float NOT NULL,
  `kode_jenis` varchar(5) NOT NULL,
  `flag` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `barang`
--

INSERT INTO `barang` (`kode_barang`, `nama_barang`, `harga_barang`, `kode_jenis`, `flag`) VALUES
('BR001', 'Laptop Asus Core i5', 4500000, 'JN002', 1),
('BR002', 'Kertas A4 1 Pack', 50000, 'JN001', 1),
('BR003', 'Xiaomi Red me 3', 1800000, 'JN001', 1),
('BR007', 'Printer Canon', 700000, 'JN00', 1),
('BR009', 'Tas Ransel', 200, 'JN002', 1);

-- --------------------------------------------------------

--
-- Table structure for table `jabatan`
--

CREATE TABLE `jabatan` (
  `kode_jabatan` varchar(5) NOT NULL,
  `nama_jabatan` varchar(100) NOT NULL,
  `keterangan` text NOT NULL,
  `flag` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `jabatan`
--

INSERT INTO `jabatan` (`kode_jabatan`, `nama_jabatan`, `keterangan`, `flag`) VALUES
('', '', '', 1),
('JB001', 'Kasir', 'Operasional', 1),
('JB002', 'Admin', 'Operasional', 1),
('JB003', 'Pramusaji', 'Operasional', 1),
('JB007', 'Staff', 'Ketua', 1),
('JB009', 'Skretaris', '', 1),
('JB010', 'Office Boy', 'Ketua', 1);

-- --------------------------------------------------------

--
-- Table structure for table `jenis_barang`
--

CREATE TABLE `jenis_barang` (
  `kode_jenis` varchar(5) NOT NULL,
  `nama_jenis` varchar(100) NOT NULL,
  `flag` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `jenis_barang`
--

INSERT INTO `jenis_barang` (`kode_jenis`, `nama_jenis`, `flag`) VALUES
('JN001', 'Alat Tulis Kantor', 1),
('JN002', 'Perangkat Keras', 1),
('', '', 1),
('JN003', 'Perangkat Lunak', 1),
('JN004', 'Bahan Kimia', 1);

-- --------------------------------------------------------

--
-- Table structure for table `karyawan`
--

CREATE TABLE `karyawan` (
  `nik` varchar(10) NOT NULL,
  `nama_lengkap` varchar(150) NOT NULL,
  `tempat_lahir` varchar(100) NOT NULL,
  `tgl_lahir` date NOT NULL,
  `jenis_kelamin` varchar(1) NOT NULL,
  `alamat` text NOT NULL,
  `telp` varchar(15) NOT NULL,
  `kode_jabatan` varchar(5) NOT NULL,
  `flag` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `karyawan`
--

INSERT INTO `karyawan` (`nik`, `nama_lengkap`, `tempat_lahir`, `tgl_lahir`, `jenis_kelamin`, `alamat`, `telp`, `kode_jabatan`, `flag`) VALUES
('1902001', 'Langgeng Kumboro', 'Lebak', '1998-12-08', 'L', 'RSN WADUK PLUIT BLOK 5 NO 302', '081511247338', 'JB001', 1),
('1902002', 'Renalldi Setiawan', 'Sumatra Selatan', '1987-01-01', 'L', 'Jl. Raya Plumpang No 29 ', '02157818912', 'JB002', 1),
('1902003', 'Usman', 'Ciamais', '0000-00-00', '0', 'Mangga Besar', '0817771922', 'JB002', 1),
('1902004', 'Ucok', 'Medan', '0000-00-00', '0', 'Kebon Jeruk', '', 'JB001', 1),
('1902005', 'Dadang', 'Bandung', '1959-01-01', '0', '', '', 'JB001', 1),
('1902008', 'Andi', '-', '1959-02-01', '0', '', '', 'JB001', 1),
('192019', 'Gandi Bagustira Subatsa', 'Tokyo', '2000-12-18', '0', 'Rawabadak Utara', '089 7770 1728', 'JB007', 1);

-- --------------------------------------------------------

--
-- Table structure for table `supplier`
--

CREATE TABLE `supplier` (
  `kode_supplier` varchar(5) NOT NULL,
  `nama_supplier` varchar(100) NOT NULL,
  `alamat` text NOT NULL,
  `telp` varchar(15) NOT NULL,
  `flag` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `supplier`
--

INSERT INTO `supplier` (`kode_supplier`, `nama_supplier`, `alamat`, `telp`, `flag`) VALUES
('SP001', 'CV. Informa Abadi', 'Jl. Mangga Dua Raya No. 10', '0213383838', 1),
('SP002', 'PT. Amarta Indah Otsuka', 'Pasuruan 67172, Jawa Timur', '08001687852', 1),
('SP004', 'PT. Senosa Makmur Tbk', 'Jl. Raya Gadog 12', '021 0291 9912', 1),
('SP009', 'Samsung Indonesia', 'Cikarang', '021 7777 9999', 1);

--
-- Indexes for dumped tables
--

--
-- Indexes for table `barang`
--
ALTER TABLE `barang`
  ADD PRIMARY KEY (`kode_barang`);

--
-- Indexes for table `jabatan`
--
ALTER TABLE `jabatan`
  ADD PRIMARY KEY (`kode_jabatan`);

--
-- Indexes for table `karyawan`
--
ALTER TABLE `karyawan`
  ADD PRIMARY KEY (`nik`);

--
-- Indexes for table `supplier`
--
ALTER TABLE `supplier`
  ADD PRIMARY KEY (`kode_supplier`);

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
