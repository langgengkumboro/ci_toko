<?php
class Supplier extends CI_Controller {

	public function __construct()
	{
		parent::__construct();
		//load model terkait
		$this->load->model("Supplier_models");
	}

	public function index()
	{
		$this->data_supplier();
	}
	public function data_supplier()
	{
		$data['data_supplier'] = $this->Supplier_models->tampilDataSupplier();
		$this->load->view('data_supplier', $data);
	}

	public function detailsupplier($kode_supplier)
	{
		$data['data_supplier'] =$this->Supplier_models->detailsupplier($kode_supplier);
		$this->load->view('detailsupplier', $data);
	}
	public function inputsupplier()
	{
		$data['data_supplier'] = $this->Supplier_models->tampilDataSupplier();

		if (!empty($_REQUEST)){
			$m_supplier = $this->Supplier_models;
			$m_supplier->save();
			redirect("Supplier/index", "refresh");
		}


		$this->load->view('inputsupplier',$data);
	}
	

}

