<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Barang extends CI_Controller {

	public function __construct()
	{
		parent::__construct();
		//load model terkait
		$this->load->model("Barang_models");
	}

	public function index()
	{
		$this->data_barang();
	}
	public function data_barang()
	{
		$data['data_barang'] = $this->Barang_models->tampilDataBarang();
		$this->load->view('data_barang', $data);
	}
	public function detailbarang($kode_barang)
	{
		$data['data_barang'] =$this->Barang_models->detail($kode_barang);
		$this->load->view('detailbarang', $data);
	}

	public function inputbarang()
	{
		$data['data_barang'] = $this->Barang_models->tampilDataBarang();

		if (!empty($_REQUEST)){
			$m_barang = $this->Barang_models;
			$m_barang->save();
			redirect("Barang/index", "refresh");
		}


		$this->load->view('inputbarang',$data);
	}
	

}
